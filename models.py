from datetime import datetime

from app import db


def slugify(str_to_slugify):
    str_to_slugify_list = str_to_slugify.lower().split()
    map_lambda = lambda str: str.replace('-', '')
    filter_lambda = lambda str: bool(str)
    return '-'.join(filter(filter_lambda, map(map_lambda, str_to_slugify_list)))


class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(140), nullable=False)
    slug = db.Column(db.String(140), unique=True)
    created_on = db.Column(db.DateTime(), default=datetime.utcnow)
    updated_on = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)
        self.generate_slug()

    def generate_slug(self):
        if self.title:
            self.slug = slugify(self.title)

    def __repr__(self):
        return "Post <id:{}, title:{}>".format(self.id, self.title)
