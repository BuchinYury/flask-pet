from flask import Blueprint
from flask import render_template

from models import Post

from articles.blueprint import index as articles_index

posts = Blueprint('posts', __name__, template_folder='templates')


@posts.route('/')
def index():
    posts = Post.query.order_by(Post.id).all()
    return render_template('posts/index.html', posts=posts)


@posts.route('/articles')
def articles():
    return articles_index()
