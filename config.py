import os


class Config(object):
    DEBUG = True
    SECRET_KEY = os.environ.get('FLASK_CONFIG_SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'FLASK_CONFIG_SQLALCHEMY_DATABASE_URI') or 'postgresql+psycopg2://postgres:example@127.0.0.1:5432/blog'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
