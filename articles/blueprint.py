from flask import Blueprint
from flask import render_template

articles = Blueprint('articles', __name__, template_folder='templates')


@articles.route('/')
def index():
    return render_template('articles/index.html', n="Oleg")
