from app import app
from posts.blueprint import posts
from articles.blueprint import articles

app.register_blueprint(posts, url_prefix='/blog')
app.register_blueprint(articles, url_prefix='/articles')

if __name__ == '__main__':
    print("app.run()", __name__)
    app.run()
